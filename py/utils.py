#!/usr/bin/env python3

import os, sys, time

LOG_TRACE = 1
LOG_INFO = 2
LOG_WARN = 4
LOG_ERROR = 8
LOGMSG_TYPES_PLAIN = {LOG_INFO:"info", LOG_TRACE:"trace", LOG_WARN:"warn", LOG_ERROR:"error"}
LOGMSG_TYPES_COLORED = {LOG_INFO:"\033[96minfo\033[0m", LOG_TRACE:"\033[39mtrace\033[0m", LOG_WARN:"\033[93mwarn\033[0m", LOG_ERROR:"\033[91merror\033[0m"}
LOGMSG_TYPES = LOGMSG_TYPES_COLORED if sys.stdout.isatty() else LOGMSG_TYPES_PLAIN
VERBOSITY = LOG_INFO | LOG_WARN | LOG_ERROR

def getargv(arg, default="", argv=sys.argv):
	if arg in argv and len(argv) > argv.index(arg)+1:
		return argv[argv.index(arg)+1]
	else:
		return default

def log(msg, msgtype=LOG_TRACE, end="\n", header=True):
	if msgtype & VERBOSITY:
		if header:
			print(time.strftime("%Y-%m-%d %H:%M:%S")+" ["+LOGMSG_TYPES_COLORED[msgtype]+"] "+msg, end=end)
		else:
			print(msg, end=end)

def format_path(path):
	path = os.path.expanduser(path)
	if path != "" and path[-1] != "/":
		path += "/"
	return path
